//
//  TVGitHubResultModelTests.m
//  Avito
//
//  Created by Valentin on 28.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TVGitHubResultModel.h"

@interface TVGitHubResultModelTests : XCTestCase {
    TVGitHubResultModel *_resultModel;
    NSDictionary *_info;
}

@end


@implementation TVGitHubResultModelTests

- (void)setUp {
    [super setUp];
    _info = @{@"login":@"Alex",
              @"url":@"https://github.com/users/url-to-alex/",
              @"avatar_url":@"https://github.com/users/url-to-alex/avatar.png"};
    _resultModel = [[TVGitHubResultModel alloc] initWithInfo:_info];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void) testGitGubResultModel {
    XCTAssertEqual(_resultModel.topTitle, _info[@"login"]);
    XCTAssertEqual(_resultModel.bottomTitle, _info[@"url"]);
    XCTAssertEqual(_resultModel.imageLink, _info[@"avatar_url"]);
}


@end
