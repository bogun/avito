//
//  TVGitHubRequestModelTests.m
//  Avito
//
//  Created by Valentin on 29.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TVGitHubRequestModel.h"
#pragma GCC diagnostic ignored "-Wundeclared-selector"

@interface TVGitHubRequestModelTests : XCTestCase {
    TVGitHubRequestModel *_requestModel;
}

@end

@implementation TVGitHubRequestModelTests

- (void)setUp {
    [super setUp];
    _requestModel = [[TVGitHubRequestModel alloc] init];
    _requestModel.searchURL = @"https://api.github.com/search/users?";
    _requestModel.q = @"Hello world";
    _requestModel.sort = @"login";
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void) testGetRequest {
    NSURLRequest *request = [_requestModel performSelector:@selector(getRequest)];
    XCTAssertNotNil(request);
}

- (void) testPreparedRequest {
    NSURLRequest *request = [_requestModel performSelector:@selector(preparedRequest)];
    XCTAssertNotNil(request);
}

- (void) testGenerateBodyFromPropertiesAndValues {
    NSString *body = [_requestModel performSelector:@selector(generateBodyFromPropertiesAndValues)];
    XCTAssertGreaterThan([body length], 0);
}

- (void) testArrayPropertiesAndValues {

    NSArray *array = [_requestModel performSelector:@selector(arrayPropertiesAndValues)];
    XCTAssertGreaterThan(array.count, 0);
}


@end
