//
//  TVITunesResultModelTests.m
//  Avito
//
//  Created by Valentin on 28.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TVITunesResultModel.h"

@interface TVITunesResultModelTests : XCTestCase {
    TVITunesResultModel *_resultModel;
    NSDictionary *_info;
}
@end


@implementation TVITunesResultModelTests

- (void)setUp {
    [super setUp];
    _info = @{@"artistName":@"Jonny",
              @"trackName":@"Hello my beautiful world",
              @"artworkUrl100":@"https://itunes.apple.com/users/url-to-jonny/avatar.png"};
    _resultModel = [[TVITunesResultModel alloc] initWithInfo:_info];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void) testITunesResultModel {
    XCTAssertEqual(_resultModel.topTitle, _info[@"artistName"]);
    XCTAssertEqual(_resultModel.bottomTitle, _info[@"trackName"]);
    XCTAssertEqual(_resultModel.imageLink, _info[@"artworkUrl100"]);
}

@end
