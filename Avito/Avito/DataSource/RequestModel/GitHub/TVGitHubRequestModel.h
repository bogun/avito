//
//  TVGitHubRequestModel.h
//  Avito
//
//  Created by Valentin Titov on 22.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVRequestModel.h"

@interface TVGitHubRequestModel : TVRequestModel
/* The property "q" is an attribute contained search string */
@property(strong, nonatomic) NSString *q;
/* The property "sort" is an attribute contained sort parameter */
@property(strong, nonatomic) NSString *sort;

@end
