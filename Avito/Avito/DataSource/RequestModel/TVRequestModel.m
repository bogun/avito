//
//  TVRequestModel.m
//  Avito
//
//  Created by Valentin on 22.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVRequestModel.h"
#import <objc/runtime.h>

@implementation TVRequestModel

- (NSURLRequest *)getRequest {
    NSError *error = nil;
    [self validationFields:&error];
    if (error) {
        NSLog(@"Error code: %ld \n%@",(long)error.code,error.userInfo);
        return nil;
    }
    NSURLRequest *request = [self preparedRequest];
    return request;
}

- (NSURLRequest*) preparedRequest {
    NSString *body = [self generateBodyFromPropertiesAndValues];
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@",self.searchURL,body];
    NSURL *url = [[NSURL alloc] initWithString:fullUrl];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    return request;
}


#pragma mark - prepare request body
- (NSString *) generateBodyFromPropertiesAndValues {
    NSArray *parametersArray = [self arrayPropertiesAndValues];
    NSString* parametersAsString = [parametersArray componentsJoinedByString:@"&"];
    return [self replaceSpaceToPlusForString: parametersAsString];
}

- (NSArray*) arrayPropertiesAndValues {
    NSMutableArray *parametersArray = [NSMutableArray array];
    unsigned int numberOfProperties = 0;
    objc_property_t *propertyArray = class_copyPropertyList([self class], &numberOfProperties);
    for (NSUInteger i = 0; i < numberOfProperties; i++) {
        objc_property_t property = propertyArray[i];
        NSString *propertyName = [self propertyAsString:property];
        if (![propertyName isEqualToString:@"searchURL"]) {
            NSString *propertyValue = [self valueForKey:propertyName];
            [parametersArray addObject:[NSString stringWithFormat:@"%@=%@",propertyName, propertyValue]];
        }
    }
    free(propertyArray);
    return parametersArray;
}

- (NSString*) replaceSpaceToPlusForString:(NSString*) string {
    return [string stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

#pragma mark - validation
- (void) validationFields:(NSError**)error {
    unsigned int numberOfProperties = 0;
    objc_property_t *propertyArray = class_copyPropertyList([self class], &numberOfProperties);
    for (NSUInteger i = 0; i < numberOfProperties; i++) {
        objc_property_t property = propertyArray[i];
        NSString *propertyName = [self propertyAsString:property];
        id propertyValue = [self valueForKey:propertyName];
        BOOL isValid = [self isExistField:propertyValue];
        if (!isValid) {
            NSString *errorBody = [NSString stringWithFormat:@"The property %@ is not valid",propertyName];
            NSError *operationError  = [[NSError alloc] initWithDomain:@"" code:404 userInfo:@{@"Error":errorBody}];
            *error = operationError;
            break;
        };
    }
    free(propertyArray);
}

- (BOOL) isExistField:(id)field {
    if (!field) {
        return NO;
    }
    return YES;
}

#pragma mark - helpful methods

- (NSString*) propertyAsString:(objc_property_t)property {
    return [[NSString alloc] initWithUTF8String:property_getName(property)];
}


@end
