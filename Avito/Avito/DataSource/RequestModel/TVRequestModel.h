//
//  TVRequestModel.h
//  Avito
//
//  Created by Valentin on 22.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
@interface TVRequestModel : NSObject

@property(strong, nonatomic) NSString *searchURL;

/** Function prepare request for search engine
 *  @return An instance of NSURLRequest with prepared info for search engine
 */
- (NSURLRequest*) getRequest;
@end
