//
//  TVITunesRequestModel.h
//  Avito
//
//  Created by Valentin Titov on 22.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVRequestModel.h"

@interface TVITunesRequestModel : TVRequestModel
/* The property "term" is an attribute contained search string */
@property(strong, nonatomic) NSString *term;
/* The property "country" is an attribute contained country as a sort parameter */
@property(strong, nonatomic) NSString *country;


@end
