//
//  TVGitHubSearchEngine.m
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVGitHubSearchEngine.h"
#import "TVGitHubRequestModel.h"
#import "TVGitHubResultModel.h"
@interface TVGitHubSearchEngine() {
    TVGitHubRequestModel *_requestModel;
}
@end


@implementation TVGitHubSearchEngine


#pragma mark - initialization
- (instancetype)init {
    self = [super init];
    if (self) {
        [self initializationRequestModel];
    }
    return self;
}

- (void) initializationRequestModel {
    _requestModel = [[TVGitHubRequestModel alloc] init];
    _requestModel.searchURL = kSearchURL;
    _requestModel.sort = @"followers";
}


#pragma mark - Search
- (void)search:(NSString *)searchText {
    _requestModel.q = searchText;
    NSURLRequest *request = [_requestModel getRequest];
    if (request) {
        [self startWithRequest:request];
    } else {
        NSLog(@"Error generation request. Please, try again");
    }
    
}


#pragma mark - Recieve result
- (void) didLoadContent:(NSDictionary*) content {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray *parsedResult = [self parsedResultForContent:content];
        [self.dataSource didParsedSearchEngineResult:parsedResult];
    });
}


#pragma mark - Parse
- (NSArray*) parsedResultForContent:(NSDictionary*) content {
    NSMutableArray *parsedResult = [NSMutableArray array];
    NSArray *notParsedResult = content[@"items"];
    for (NSDictionary *info in notParsedResult) {
        TVGitHubResultModel *model = [[TVGitHubResultModel alloc] initWithInfo:info];
        [parsedResult addObject:model];
    }
    return parsedResult;
}



@end
