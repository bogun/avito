//
//  TVSearchManager.h
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TVSearchManagerDataSource <NSObject>
/** Call when data loaded and parsed
 *  @param result An array with result info
 */
- (void) didReceiveSearchResult:(NSArray*)result;

@end


@interface TVSearchManager : NSObject

@property(assign, nonatomic) id <TVSearchManagerDataSource> dataSource;
/** Singleton
 *  @return instance of singleton
 */
+ (instancetype) searchManager;
/** Make search engine and execute search with text
 *  @param searchIn A string contain name of seach engine
 *  @param searchText A string contain text for search
 */
- (void) searchIn:(NSString*)searchIn
         withText:(NSString*)searchText;

@end
