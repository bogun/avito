//
//  TVResultModel.h
//  Avito
//
//  Created by Valentin Titov on 23.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVResultModel : NSObject

/* Property has link to image, which will present as cell icon */
@property(strong, nonatomic) NSString *imageLink;
/* Property containt title depending on type of seach engine */
@property(strong, nonatomic) NSString *topTitle;
/* Property containt parameter depending on type of seach engine */
@property(strong, nonatomic) NSString *bottomTitle;
/** Initialize result model with parameters dictionary
 *  @param  info A dictionary which contain needs parameters
 *  @return An instance of result model
 */
- (instancetype)initWithInfo:(NSDictionary*)info;

@end
