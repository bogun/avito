 //
//  TVViewController.m
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVViewController.h"
#import "TVUIConst.h"
#import "TVSegmentedControl.h"
#import "TVMainView.h"
#import "TVTableViewCell.h"
#import "TVActivityLoaderView.h"
#import "TVSearchManager.h"
#import "TVResultModel.h"
#import "Reachability.h"

static NSString* const kITunes = @"iTunes";
static NSString* const kGitHub = @"GitHub";

@interface TVViewController () <
UITableViewDataSource,
UITableViewDelegate,
UISearchBarDelegate,
UIScrollViewDelegate,
TVSearchManagerDataSource > {

    UISegmentedControl *_segmentedControl;
    UISearchBar *_searchBar;
    UITableView *_tableView;
    NSArray *_resultList;
}

@property (nonatomic) Reachability *internetReachability;

@end


@implementation TVViewController

#pragma mark - Initialization controller
-(void)viewDidLoad {
    [super viewDidLoad];
    [self showMainView];
    [self showSegmenedControl];
    [self initializationSearchManager];
    [self initializationReachability];
    [self addObservers];
}

- (void) showSegmenedControl {
    _segmentedControl = [[TVSegmentedControl alloc] initWithItems:@[kITunes, kGitHub]];
    [_segmentedControl addTarget:self
                          action:@selector(didChangeSegmentControl)
                forControlEvents:UIControlEventValueChanged];
    self.navigationItem.titleView = _segmentedControl;
}

- (void) showMainView {
    TVMainView *mainView = [[TVMainView alloc] init];
    [self.view addSubview: mainView];
    [mainView showAllElements];
    // Delegate tableView
    _tableView = (UITableView*)mainView.tableView;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    // Delegate search bar
    _searchBar = (UISearchBar *)mainView.searchBar;
    _searchBar.delegate = self;
    // Align controller view under UINavigation bar
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void) initializationSearchManager {
    [TVSearchManager searchManager].dataSource = self;
}

- (void) initializationReachability {
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
}

- (void) addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkInternetConnection:)
                                                 name:kReachabilityChangedNotification object:nil];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _resultList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    TVTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[TVTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    // Show cell
    if ([[self currentSegment] isEqualToString:kITunes]) {
        indexPath.row % 2 == 0 ? [cell showITunesOddCell]:[cell showITunesEvenCell];
    } else if ([[self currentSegment]isEqualToString:kGitHub]) {
        indexPath.row % 2 == 0 ? [cell showGitHubOddCell]:[cell showGitHubEvenCell];
    }
    
    // Load data to cell
    TVResultModel *infoModel = _resultList[indexPath.row];
    [cell setTopLabelText:infoModel.topTitle
          bottomLabelText:infoModel.bottomTitle];
    [cell.avatarView setImage:nil forState:UIControlStateNormal];
    NSURL *url = [NSURL URLWithString:infoModel.imageLink];
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    TVTableViewCell *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                    if (updateCell) {
                        [updateCell.avatarView setImage:image forState:UIControlStateNormal];
                        [updateCell.avatarView addTarget:self action:@selector(avatarZoomIn:) forControlEvents:UIControlEventTouchUpInside];
                    }
                });
            }
        }
    }];
    [task resume];
    return cell;
}



#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kDefaultCellHeight;
}


#pragma mark - UISearchBarDelegate methods
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    NSString *searchIn = [self currentSegment];
    [self startSearchIn:searchIn withSearchText:searchBar.text];
}

- (NSString*) currentSegment {
    NSInteger selectedIndex =  _segmentedControl.selectedSegmentIndex;
    return [_segmentedControl titleForSegmentAtIndex:selectedIndex];
}

- (void) didChangeSegmentControl {
    [self startSearchIn:[self currentSegment] withSearchText:_searchBar.text];
}

- (void) startSearchIn:(NSString*)searchIn withSearchText:(NSString*)text {
    if ([self.internetReachability currentReachabilityStatus] != NotReachable) {
        [TVActivityLoaderView showActivityLoaderOnView];
        [[TVSearchManager searchManager] searchIn:searchIn withText:text];
    } else {
        [self showAlertWithTitle:@"Warning!"
                         meesage:@"Internet is not available now. Please, check connection"];
    }
}

#pragma mark - TVDataSource methods
- (void) didReceiveSearchResult:(NSArray*) result {
    [self checkRecievedResult:result];
    [TVActivityLoaderView hideActivityLoader];
}

- (void) checkRecievedResult:(NSArray*) result {
    if (result.count > 0) {
        _resultList = result;
        [_tableView reloadData];
    } else {
        NSString *message = [NSString stringWithFormat:@"The %@ didn't find anything for your request: %@",[self currentSegment],_searchBar.text];
        [self showAlertWithTitle:@"Warning!" meesage:message];
    }
}


#pragma mark - UIScrollViewDelegate methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self finishScrolling];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self finishScrolling];
}

- (void)finishScrolling {
    NSArray *visibleCells = [_tableView visibleCells];
    for (NSInteger index = 0; index < visibleCells.count; index++) {
        TVTableViewCell *cell = visibleCells[index];
        if ([[self currentSegment] isEqualToString:kITunes]) {
            index % 2 == 0 ? [cell showITunesOddCell]:[cell showITunesEvenCell];
        } else if ([[self currentSegment]isEqualToString:kGitHub]) {
            index % 2 == 0 ? [cell showGitHubOddCell]:[cell showGitHubEvenCell];
        }
    }
}


#pragma mark - ZoomIn/ZoomOut avatar
- (void) avatarZoomIn:(UIButton*)sender {
    sender.selected = YES;
    //Get current button position
    CGRect avatarRect = [self convertConstraintsToFramesForElement:sender];
    //Iniialization and prepare image view for animate
    UIImageView *avatarView = [[UIImageView alloc] initWithFrame:avatarRect];
    [avatarView setImage:sender.imageView.image];
    [self addTapGestureToView:avatarView withSelector:@selector(avatarZoomOut:)];
    [UIView animateWithDuration:kDefaultAnimationDuration animations:^{
        [self showOnFullScreenAvatar:avatarView];
    } completion:^(BOOL finished) {
        // Add constraints for good represent when rotation device
        avatarView.translatesAutoresizingMaskIntoConstraints = NO;
        [[avatarView tv_pinAllEdgesToSameEdgesOfSuperView] tv_installConstraints];
    }];
}

- (void) showOnFullScreenAvatar:(UIImageView*)avatarView {
    UIView *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:avatarView];
    [avatarView setFrame:window.frame];
}

- (void) avatarZoomOut:(UITapGestureRecognizer*)tapGesture {
    __block UIImageView *avatarView = (UIImageView *)tapGesture.view;
    [self resetConstraintsForElement:avatarView];
    TVTableViewCell *cell = [self selectedButtonCell];
    if (cell) {
        CGRect avatarRect = [self convertConstraintsToFramesForElement:cell.avatarView];
        [UIView animateWithDuration:kDefaultAnimationDuration animations:^{
            [avatarView setFrame:avatarRect];
        } completion:^(BOOL finished) {
            [avatarView removeFromSuperview];
            avatarView = nil;
            cell.avatarView.selected = NO;
        }];
    } else {
        NSLog(@"Error. Couldn't find cell with selected button");
    }

}

#pragma mark - Reachabilyty methods

- (void) checkInternetConnection:(NSNotification*) notification {
    if ([self.internetReachability currentReachabilityStatus] == NotReachable) {
        NSLog(@"Lost internet connection");
        [self showAlertWithTitle:@"Warning!"
                         meesage:@"Internet is not available now. Please, check connection"];
    } else {
        NSLog(@"Internet connection established");
        [self showAlertWithTitle:@"Сongratulations!"
                         meesage:@"Internet connection available now!"];
    }
}

#pragma mark - Dealloc

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - helpful methods

- (void) addTapGestureToView:(UIImageView*)view
                withSelector:(SEL)selector {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:selector];
    tap.numberOfTapsRequired = 1;
    [view addGestureRecognizer:tap];
    view.userInteractionEnabled = YES;
}

- (void) resetView:(UIImageView*)view
    withTapGesture:(UITapGestureRecognizer*)tapGesture {
    [view removeFromSuperview];
    [view removeGestureRecognizer:tapGesture];
}

- (CGRect) convertConstraintsToFramesForElement:(UIView*)element {
    UIView *window = [UIApplication sharedApplication].keyWindow;
    CGRect rect = [element convertRect:element.bounds toView:window];
    return rect;
}

- (void) resetConstraintsForElement:(UIView*) element {
    [element removeConstraints:[element constraints]];
}

- (TVTableViewCell*) selectedButtonCell {
    NSArray *visibleCells = [_tableView visibleCells];
    for (TVTableViewCell *cell in visibleCells) {
        if (cell.avatarView.selected == YES) {
            return cell;
        }
    }
    return nil;
}

- (void) showAlertWithTitle:(NSString*)title
                    meesage:(NSString*) message {
    [[[UIAlertView alloc] initWithTitle:title
                                message:message
                               delegate:self
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles: nil] show];
}

@end
