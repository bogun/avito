//
//  TVMainView.m
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVMainView.h"
#import "TVSearchBar.h"
#import "TVTableView.h"
#import "TVUIConst.h"
#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"

@implementation TVMainView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        [self initSearchBar];
        [self initTableView];
    }
    return self;
}


#pragma mark - init all elements

- (void) initSearchBar {
    _searchBar = [[TVSearchBar alloc] init];
    [self addSubview: self.searchBar];
    _searchBar.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void) initTableView {
    _tableView = [[TVTableView alloc] init];
    [self addSubview:self.tableView];
    _tableView.translatesAutoresizingMaskIntoConstraints = NO;
}

#pragma mark - show elements position
- (void) showAllElements {
    [self setSelfPosition];
    [self setSearchBarPosition];
    [self setTableViewPosition];
}

- (void) setSelfPosition {
    [self tv_resetConstraints];
    [[self tv_pinAllEdgesToSameEdgesOfSuperView]tv_installConstraints];

}

- (void) setSearchBarPosition {
    [_searchBar tv_resetConstraints];
    [[_searchBar tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeTop|UIRectEdgeLeft|UIRectEdgeRight] tv_installConstraints];
}

- (void) setTableViewPosition {
    [_tableView tv_resetConstraints];
    [[_tableView tv_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeBottom ofView:_searchBar] tv_install];
    [[_tableView tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeLeft|UIRectEdgeBottom|UIRectEdgeRight
                                        withInsets:UIEdgeInsetsMake(0.0f, kDefaultPadding, -kDefaultPadding, -kDefaultPadding)] tv_installConstraints];
}





@end
