//
// Created by Valentin Titov on 15.02.15.
// Copyright (c) 2015 Anton Dobkin. All rights reserved.
//

#import "UIView+AutoLayout.h"

@implementation UIView (AutoLayout)

+ (instancetype)tv_viewWithAutoLayout {
    UIView *view = [self new];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    return view;
}

#pragma mark - Add constraints to sides

- (NSLayoutConstraint *)tv_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset relation:(NSLayoutRelation)relation {
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:[self __tv_edgeToLayoutAttribute:edge]
                                                                  relatedBy:relation
                                                                     toItem:view
                                                                  attribute:[self __tv_edgeToLayoutAttribute:toEdge]
                                                                 multiplier:1.0f
                                                                   constant:inset];
    return constraint;
}

- (NSLayoutConstraint *)tv_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view withInset:(CGFloat)inset {
    return [self tv_pinEdge:edge toEdge:toEdge ofView:view withInset:inset relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)tv_pinEdge:(UIRectEdge)edge toEdge:(UIRectEdge)toEdge ofView:(UIView *)view {
    return [self tv_pinEdge:edge toEdge:toEdge ofView:view withInset:0.0f relation:NSLayoutRelationEqual];
}

- (NSArray *)tv_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges withInsets:(UIEdgeInsets)insets {
    NSAssert(self.superview, @"self.superview is nil");
    NSMutableArray *constraints = [NSMutableArray array];
    NSLayoutConstraint *constraint = nil;
    if (edges & UIRectEdgeTop) {
        constraint = [self tv_pinEdge:UIRectEdgeTop toEdge:UIRectEdgeTop ofView:self.superview withInset:insets.top];
        [constraints addObject:constraint];
    }
    if (edges & UIRectEdgeLeft) {
        constraint = [self tv_pinEdge:UIRectEdgeLeft toEdge:UIRectEdgeLeft ofView:self.superview withInset:insets.left];
        [constraints addObject:constraint];
    }
    if (edges & UIRectEdgeBottom) {
        constraint = [self tv_pinEdge:UIRectEdgeBottom toEdge:UIRectEdgeBottom ofView:self.superview withInset:insets.bottom];
        [constraints addObject:constraint];
    }
    if (edges & UIRectEdgeRight) {
        constraint = [self tv_pinEdge:UIRectEdgeRight toEdge:UIRectEdgeRight ofView:self.superview withInset:insets.right];
        [constraints addObject:constraint];
    }
    return constraints;
}

- (NSArray *)tv_pinEdgesToSameEdgesOfSuperView:(UIRectEdge)edges {
    return [self tv_pinEdgesToSameEdgesOfSuperView:edges withInsets:UIEdgeInsetsZero];
}

- (NSArray *)tv_pinAllEdgesToSameEdgesOfSuperView:(UIEdgeInsets)insets {
    return [self tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeAll withInsets:insets];
}

- (NSArray *)tv_pinAllEdgesToSameEdgesOfSuperView {
    return [self tv_pinEdgesToSameEdgesOfSuperView:UIRectEdgeAll];
}

- (NSLayoutConstraint *)tv_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset relation: (NSLayoutRelation) relation {
    NSAssert(self.superview, @"self.superview is nil");
    return [self tv_pinEdge:edge toEdge:edge ofView:self.superview withInset:inset relation:relation];
}

- (NSLayoutConstraint *)tv_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge withInset: (CGFloat) inset {
    return [self tv_pinEdgeToSameEdgeOfSuperView: edge withInset:inset relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)tv_pinEdgeToSameEdgeOfSuperView:(UIRectEdge)edge {
    return [self tv_pinEdgeToSameEdgeOfSuperView: edge withInset:0.0f];
}

#pragma mark - Align relative to center

- (NSLayoutConstraint *)tv_toAlignOnAxis:(ADAxis)axis withInset:(CGFloat)inset {
    NSAssert(self.superview, @"%@: superview must not be nil", self);
    NSLayoutAttribute attribute = [self __tv_axisToAttribute:axis];
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self
                                                                  attribute:attribute
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.superview
                                                                  attribute:attribute
                                                                 multiplier:1.0f
                                                                   constant:inset];

    return constraint;
}

- (NSLayoutConstraint *)tv_toAlignOnAxisYWithInset:(CGFloat)inset {
    return [self tv_toAlignOnAxis:ADAxisY withInset:inset];
}

- (NSLayoutConstraint *)tv_toAlignOnAxisXWithInset:(CGFloat)inset {
    return [self tv_toAlignOnAxis:ADAxisY withInset:inset];
}

- (NSLayoutConstraint *)tv_toAlignOnAxisY {
    return [self tv_toAlignOnAxis:ADAxisY withInset:0.0f];
}

- (NSLayoutConstraint *)tv_toAlignOnAxisX {
    return [self tv_toAlignOnAxis:ADAxisX withInset:0.0f];
}

#pragma mark - Work with view size

- (NSLayoutConstraint *)tv_height:(CGFloat)height relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeHeight
                                        relatedBy:relation
                                           toItem:nil
                                        attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1.0f
                                         constant:height];
}

- (NSLayoutConstraint *)tv_height:(CGFloat)height {
    return [self tv_height:height relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)tv_width:(CGFloat)width relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeWidth
                                        relatedBy:relation
                                           toItem:nil
                                        attribute:NSLayoutAttributeNotAnAttribute
                                       multiplier:1.0f
                                         constant:width];
}

- (NSLayoutConstraint *)tv_width:(CGFloat)width {
    return [self tv_width:width relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)tv_heightEqualToHeightOfView:(UIView *)view relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeHeight
                                        relatedBy:relation
                                           toItem:view
                                        attribute:NSLayoutAttributeHeight
                                       multiplier:1.0f
                                         constant:0.0f];
}

- (NSLayoutConstraint *)tv_heightEqualToHeightOfView:(UIView *)view {
    return [self tv_heightEqualToHeightOfView:view relation:NSLayoutRelationEqual];
}

- (NSLayoutConstraint *)tv_widthEqualToWidthOfView:(UIView *)view relation:(NSLayoutRelation)relation {
    return [NSLayoutConstraint constraintWithItem:self
                                        attribute:NSLayoutAttributeWidth
                                        relatedBy:relation
                                           toItem:view
                                        attribute:NSLayoutAttributeWidth
                                       multiplier:1.0f
                                         constant:0.0f];
}

- (NSLayoutConstraint *)tv_widthEqualToWidthOfView:(UIView *)view {
    return [self tv_widthEqualToWidthOfView:view relation:NSLayoutRelationEqual];
}

#pragma mark - Private Methods

- (UIView *)tv_commonSuperViewWithView:(UIView *)view {
    UIView *commonSuperview = nil;
    UIView *startView = self;
    do {
        if ([view isDescendantOfView:startView]) {
            commonSuperview = startView;
        }
        startView = startView.superview;
    } while (startView && !commonSuperview);
    NSAssert(commonSuperview, @"%@, %@: Does't have a common superview. Both views must be added into the same view hierarchy.", self, view);
    return commonSuperview;
}

- (NSLayoutAttribute)__tv_edgeToLayoutAttribute:(UIRectEdge)edge {
    NSLayoutAttribute attribute = NSLayoutAttributeNotAnAttribute;
    switch (edge) {
        case UIRectEdgeLeft:
            attribute = NSLayoutAttributeLeft;
            break;
        case UIRectEdgeRight:
            attribute = NSLayoutAttributeRight;
            break;
        case UIRectEdgeBottom:
            attribute = NSLayoutAttributeBottom;
            break;
        case UIRectEdgeTop:
            attribute = NSLayoutAttributeTop;
            break;
        default:
            break;
    }
    return attribute;
}

- (NSLayoutAttribute)__tv_axisToAttribute:(ADAxis)axis {
    NSLayoutAttribute attribute = NSLayoutAttributeNotAnAttribute;
    switch (axis) {
        case ADAxisX:
            attribute = NSLayoutAttributeCenterX;
            break;
        case ADAxisY:
            attribute = NSLayoutAttributeCenterY;
            break;
    }
    return attribute;
}

#pragma mark - reset all constraints
- (void) tv_resetConstraints {
    [self removeConstraints:[self constraints]];
}

@end
