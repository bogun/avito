//
// Created by Valentin Titov on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NSArray (AutoLayout)
- (void)tv_installConstraints;
- (void)tv_removeConstraints;

@end
