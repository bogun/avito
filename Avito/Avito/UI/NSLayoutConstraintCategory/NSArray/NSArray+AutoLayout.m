//
// Created by Valentin Titov on 15.02.15.
// Copyright (c) 2015 Browsec. All rights reserved.
//

#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"

@implementation NSArray (AutoLayout)

- (void)tv_installConstraints {
    [NSLayoutConstraint tv_installConstraints:self];
}

- (void)tv_removeConstraints {
    [NSLayoutConstraint tv_removeConstraints:self];
}


@end
