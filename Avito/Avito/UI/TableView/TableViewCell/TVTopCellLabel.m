//
//  TVTopCellLabel.m
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVTopCellLabel.h"
#import "TVUIConst.h"

@implementation TVTopCellLabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStyle];
    }
    return self;
}

- (void)setStyle {
    [super setStyle];
    [self setFont:[UIFont fontWithName:kHeaderFont size:kDefaultFontSize]];
}

@end
