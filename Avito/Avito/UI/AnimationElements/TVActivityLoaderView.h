//
//  TVActivityLoaderView.h
//
//  Copyright © 2016 TV. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kActivityLoaderBackroundColor [UIColor colorWithWhite: 0.0f alpha:0.8f]

@interface TVActivityLoaderView : UIView

+ (void) showActivityLoaderOnView;
+ (void) hideActivityLoader;

@end
