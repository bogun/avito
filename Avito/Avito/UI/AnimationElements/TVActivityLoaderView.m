//
//  TVActivityLoaderView.m
//
//  Copyright © 2016 TV. All rights reserved.
//

#import "TVActivityLoaderView.h"
#import "NSArray+AutoLayout.h"
#import "NSLayoutConstraint+AutoLayout.h"
#import "UIView+AutoLayout.h"

static UIView *_window;
static UIView* _backgroundView = nil;
static UIActivityIndicatorView* _indicator = nil;



@implementation TVActivityLoaderView

#pragma mark - initialization elements
+ (void) showActivityLoaderOnView {
    _backgroundView = [[self class] initializationBackgroundView];
    _window =[UIApplication sharedApplication].keyWindow;
    [_window addSubview:_backgroundView];
    _indicator = [[self class] initializationActivityIndicatior];
    [_backgroundView addSubview:_indicator];
    [[self class] setBackgroundViewPosition];
    [[self class] setIndicatorPosition];
    [UIView animateWithDuration:0.7f animations:^{
        _backgroundView.alpha = 1.0f;
        _indicator.alpha = 1.0f;
    }];
}

+ (UIView*) initializationBackgroundView {
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = kActivityLoaderBackroundColor;
    _backgroundView.alpha = 0.0f;
    return backgroundView;
}

+ (UIActivityIndicatorView*) initializationActivityIndicatior {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicator startAnimating];
    _indicator.alpha = 0.0f;
    return indicator;
}


#pragma mark - set elements position
+ (void) setBackgroundViewPosition {
    _backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
    [[_backgroundView tv_pinAllEdgesToSameEdgesOfSuperView] tv_installConstraints];
}

+ (void) setIndicatorPosition {
    _indicator.translatesAutoresizingMaskIntoConstraints = NO;
    [[_indicator tv_toAlignOnAxisX] tv_install];
    [[_indicator tv_toAlignOnAxisY] tv_install];
}


#pragma mark - hide all loader elements
+ (void) hideActivityLoader {
    [UIView animateWithDuration:0.7f animations:^{
        _indicator.alpha = 0.0f;
        _backgroundView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [_indicator removeFromSuperview];
        [_backgroundView removeFromSuperview];
        _indicator = nil;
        _backgroundView = nil;
    }];
}



@end
