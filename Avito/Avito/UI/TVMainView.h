//
//  TVMainView.h
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TVTableView, TVSearchBar;
@interface TVMainView : UIView

@property(strong, nonatomic) TVSearchBar *searchBar;
@property(strong, nonatomic) TVTableView *tableView;

- (void) showAllElements;

@end
