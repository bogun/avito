//
//  TVSegmentedControl.m
//  Avito
//
//  Created by Valentin on 21.07.16.
//  Copyright © 2016 Titov Valentin. All rights reserved.
//

#import "TVSegmentedControl.h"
#import "TVUIConst.h"

@implementation TVSegmentedControl

- (instancetype)initWithItems:(nullable NSArray *)items {
    self = [super initWithItems:items];
    if (self) {
        [self setStyle];
        [self setDefaultSettings];
    }
    return self;
}

- (void) setStyle {
    self.tintColor = kDefaultColor;
}

- (void) setDefaultSettings {
    self.selectedSegmentIndex = 0;
}

- (void)setSelectedSegmentIndex:(NSInteger)index {
    [super setSelectedSegmentIndex:index];
// Call action for event UIControlEventValueChanged
    if (index == self.selectedSegmentIndex) {
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }    
}

@end
